import json
import os
from utils import download_image
import redis
from rq import Queue


def download_cards():
    """ Download all cards from the Pokemon TCG API """
    db = redis.Redis(host='localhost', port=6379, db=0)
    rq_redis = redis.Redis(host='localhost', port=6379, db=1)
    # db.flushdb()
    q = Queue(connection=rq_redis)
    base_path = "/home/alexander/Proyectos/pokemon-tcg-data"
    json_files = os.listdir(base_path + "/cards/en/")

    with open(base_path + "/sets/en.json") as sets_file:
        _sets = json.load(sets_file)
        sets = {}
        for _set in _sets:
            sets[_set["id"]] = _set
    total = 0
    for file in json_files:
        with open(base_path + "/cards/en/" + file) as f:
            cards = json.load(f)

            for card in cards:
                total = total + 1
                card["set"] = sets[card["id"].split("-")[0]]

                abilities = []
                if card.get("abilities"):
                    abilities = [
                        {
                            "name": ability["name"],
                            "text": ability["text"],
                            "type": ability["type"]
                        } for ability in card["abilities"]]
                attacks = []
                if card.get("attacks"):
                    attacks = [
                        {
                            "name": attack["name"],
                            "cost": attack["cost"],
                            "convertedEnergyCost": attack["convertedEnergyCost"],
                            "damage": attack["damage"],
                            "text": attack.get("text", "")
                        } for attack in card["attacks"]]

                db.set(card["id"], json.dumps({
                    "name": card["name"],
                    "supertype": card.get("supertype"),
                    "subtypes": card.get("subtypes"),
                    "level": card.get("level"),
                    "hp": card.get("hp"),
                    "types": card.get("types"),
                    "evolvesFrom": card.get("evolvesFrom"),
                    "evolvesTo": card.get("evolvesTo"),
                    "abilities": abilities,
                    "attacks": attacks,
                    "set": {
                        "id": card["set"]["id"],
                        "name": card["set"]["name"],
                        "printedTotal": card["set"]["printedTotal"]
                    } if card.get("set") else None,
                    "number": card["number"],
                    "images": {
                        "small": card["images"]["small"],
                        "large": card["images"]["large"]
                    },
                    "ancientTrait": {"name": card["ancientTrait"]["name"],
                                     "text": card["ancientTrait"]["text"]} if card.get("ancientTrait") else None,
                    "rules": card.get("rules"),
                    "weaknesses": [{"type": weakness["type"], "value": weakness["value"]} for weakness in
                                   card["weaknesses"]] if card.get("weaknesses") else [],
                    "resistances": [{"type": resistance["type"], "value": resistance["value"]} for resistance in
                                    card["resistances"]] if card.get("resistances") else [],
                    "retreatCost": card.get("retreatCost"),
                    "convertedRetreatCost": card.get("convertedRetreatCost"),
                    "artist": card.get("artist"),
                    "rarity": card.get("rarity"),
                    "flavorText": card.get("flavorText"),
                    "nationalPokedexNumbers": card.get("nationalPokedexNumbers"),
                    "legalities": {
                        "unlimited": card["legalities"].get("unlimited"),
                        "expanded": card["legalities"].get("expanded"),
                        "standard": card["legalities"].get("standard")
                    },
                    "regulationMark": card.get("regulationMark"),

                }))
                q.enqueue(download_image, card["images"]["small"], f'cards/low/{card["id"]}.png')
                q.enqueue(download_image, card["images"]["large"], f'cards/hires/{card["id"]}.png')

    print("total::: ", total)


if __name__ == "__main__":
    download_cards()
