import requests


def download_image(image_url, filename):
    with open(filename, "wb") as f:
        f.write(requests.get(image_url).content)
